﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ClipperLib;

using Vector2i = ClipperLib.IntPoint;
using Polygon = System.Collections.Generic.List<ClipperLib.IntPoint>;

public class PolygonClipper : MonoBehaviour
{
    #region Declarations
    [SerializeField] List<Vector2> edge = new List<Vector2>();
    Polygon polygon = new Polygon();
    public CarvingSubjectMesh generator;
    public Bounds bounds;
    float length;
    float radius;

    #endregion

    #region Methods
    void ResetShape()
    {
        polygon.Clear();
        Vector2i p1 = new Vector2(-length / 2, -radius).ToVector2i();
        Vector2i p2 = new Vector2(length / 2, 0f).ToVector2i();
        polygon.Add(new Vector2i(p1.X, p2.Y));
        polygon.Add(new Vector2i(p1.X, p1.Y));
        polygon.Add(new Vector2i(p2.X, p1.Y));
        polygon.Add(new Vector2i(p2.X, p2.Y));

        ExtractEdgeFromPolygon();
    }

    void ExtractEdgeFromPolygon()
    {
        edge.Clear();
        if (polygon == null) return;

        for (int i = 0; i < polygon.Count; i++)
        {
            edge.Add(polygon[i].ToVector2());
        }
    }

    public void Clip(CuttingTool cutter)
    {
        List<Polygon> result = new List<Polygon>();
        Clipper clipper = new Clipper();
        clipper.AddPolygon(polygon, PolyType.ptSubject);
        clipper.AddPolygon(cutter.clipPolygon, PolyType.ptClip);

        clipper.Execute(ClipType.ctDifference, result, PolyFillType.pftNonZero, PolyFillType.pftNonZero);
        polygon = result[0];
        Clipper.SimplifyPolygon(polygon);

        ExtractEdgeFromPolygon();
    }

    public bool CutterWithinBounds(CuttingTool cutter)
    {
        return bounds.Intersects(cutter.bounds);
    }
    #endregion

    #region Callbacks
    void Start()
    {
        //length = generator.length;
        //radius = generator.radius;
        bounds.size = new Vector2(length, radius);
        bounds.center = generator.transform.position + Vector3.down * radius / 2;
        if (polygon == null || polygon.Count <= 2) // If we only have one edge or less, we want to reset the shape.
        {
            ResetShape();
        }
        else
        {
            ExtractEdgeFromPolygon();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(bounds.center, bounds.size);

        if (edge.Count > 0)
        {
            for (int i = 0; i < edge.Count-1; i++)
            {
                Gizmos.color = Color.white;
                Gizmos.DrawLine(edge[i].ToVector3(), edge[i + 1].ToVector3());
            }
        }
    }
    #endregion
}
