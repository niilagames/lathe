﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitWithinFrame : MonoBehaviour
{
    #region Declarations
    public CarvingController controller;
    Camera camera;
    bool finalPosition = false;
    Vector2 leftPoint, rightPoint;
    #endregion

    #region Methods
    void FitSubject()
    {
        camera.fieldOfView += 0.2f;
        if (leftPoint.x < 0 && rightPoint.x > 1)
        {
            FitSubject();
        }
    }
    #endregion

    #region Callbacks
    void Start()
    {
        camera = GetComponent<Camera>();
        leftPoint = camera.WorldToViewportPoint(new Vector3(-controller.length / 2, 0, controller.radius));
        rightPoint = camera.WorldToViewportPoint(new Vector3(controller.length / 2, 0, controller.radius));
        if (leftPoint.x < 0 && rightPoint.x > 1)
        {
            FitSubject();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion
}
