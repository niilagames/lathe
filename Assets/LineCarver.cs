﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class LineCarver : MonoBehaviour
{
    // basically: placer punkter på en linje (detect klik med edge collider?), lad spiller lave nye, flyt dem rundt for at generere profile. Send positions til generator. Stadig uklart om carving kan gøres nemt uden clipper. Men måske er det ok bare at bruge clipper?

    #region Declarations
    public CarvingSubjectMesh generator;
    public float radius;
    public float length;
    public Bounds bounds;
    BoxCollider2D boxCollider;
    public GameObject handlePrefab;
    public LayerMask layers;
    public Color withinBoundsColor;
    public Color outOfBoundsColor;

    

    public List<Handle> handles = new List<Handle>();
    public Vector2[] positions;

    public delegate void PositionEvent(Handle handle);
    public static PositionEvent PositionChanged;
    public static PositionEvent EnteredBounds;
    public static PositionEvent LeftBounds;

    #endregion

    #region Methods

    void SortHandlesListByX()
    {
        handles.Sort((h1, h2) => h1.transform.position.x.CompareTo(h2.transform.position.x));
    }

    void UpdatePositions(Handle handle)
    {
        SortHandlesListByX();
        positions = GetNormalizedHandlePositions(handles, length, radius);
        //generator.UpdateMesh(positions);
    }

    void IncludeHandle(Handle handle)
    {
        if (!handles.Contains(handle))
        {
            handles.Add(handle);
            handle.sr.color = withinBoundsColor;
            UpdatePositions(handle);
        }
    }

    void ExcludeHandle(Handle handle)
    {
        if (handles.Contains(handle))
        {
            handles.Remove(handle);
            handle.sr.color = outOfBoundsColor;
            UpdatePositions(handle);
        }
    }

    Vector2[] GetNormalizedHandlePositions(List<Handle> _handles, float _length, float _radius)
    {
        Vector2[] normalizedPos = new Vector2[_handles.Count];
        for (int i = 0; i < _handles.Count; i++)
        {
            Vector2 pos = _handles[i].transform.position;
            pos.x = (pos.x + length / 2) / length;
            pos.y /= -radius;
            normalizedPos[i] = pos;
        }

        return normalizedPos;
    }

    void CreateHandle(Vector3 position, bool updatePositions = true)
    {
        Handle newHandle = NewHandle(position, true);
    }

    Handle NewHandle(Vector3 position, bool updatePositions=true)
    {
        GameObject handleObject = Instantiate(handlePrefab, position, Quaternion.identity);
        Handle _handle = handleObject.GetComponent<Handle>();
        _handle.Initialize(bounds);
        if (_handle.IsWithinBounds())
        {
            handles.Add(_handle);
            if (updatePositions) UpdatePositions(_handle);
            _handle.sr.color = withinBoundsColor;
        }
        else
        {
            _handle.sr.color = outOfBoundsColor;
        }

        return _handle;
    }
    #endregion

    #region Callbacks
    private void OnEnable()
    {
        LineCarver.PositionChanged += UpdatePositions;
        LineCarver.EnteredBounds += IncludeHandle;
        LineCarver.LeftBounds += ExcludeHandle;
    }

    private void OnDisable()
    {
        LineCarver.PositionChanged -= UpdatePositions;
        LineCarver.EnteredBounds -= IncludeHandle;
        LineCarver.LeftBounds -= ExcludeHandle;
    }

    void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        //radius = generator.radius;
        //length = generator.length;
        bounds.size = boxCollider.size = new Vector3(length, radius);
        bounds.center = generator.transform.position + Vector3.down * radius/2;
        boxCollider.offset = generator.transform.position + Vector3.down * radius/2;
        NewHandle(new Vector3(-bounds.extents.x, -bounds.extents.y*2, 0), false);
        NewHandle(new Vector3(bounds.extents.x, -bounds.extents.y*2, 0));
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && UnityEngine.EventSystems.EventSystem.current != null &&
            UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject == null)
        {

            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z)), Vector3.forward, 100f, layers);
            Debug.DrawRay(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z)), Vector3.forward * 1000, Color.yellow, 3f);
            if (hit.collider != null)
            {
                Debug.Log(hit.transform.gameObject.name);
                if (hit.transform.gameObject.tag == "Handle")
                {
                    Debug.Log("hehe2");
                    hit.transform.GetComponent<Handle>().isPressed = true;
                }
            }
            else
            {
                Handle newHandle = NewHandle(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z)));
                newHandle.isPressed = true;
            }
        }
    }

    private void OnDrawGizmos()
    {

    }
    #endregion
}
