﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[ExecuteAlways]
public class HausdorffExample : MonoBehaviour
{
    // TODO: Potentially remove later.
    public struct PositionValuePair
    {
        public PositionValuePair(Vector2 pos, float dist)
        {
            Position = pos;
            Distance = dist;
        }

        public Vector2 Position { get; }
        public float Distance { get; }
    }

    #region Declarations
    public PolyShape shapeA;
    public PolyShape shapeB;
    public Vector2[] a, b, aClosest, bClosest;
    public float hausdorffDistance;
    public float maxAvgMinDistance;
    Color aColor, bColor;
    #endregion

    #region Methods
    
   void UpdateHausdorffDistance()
    {
        hausdorffDistance = Accuracy.HausdorffDistance(a, b, out aClosest, out bClosest);
        Debug.Log(hausdorffDistance);
    }

    void Init()
    {
        aColor = shapeA.color;
        aColor.a = 0.5f;
        bColor = shapeB.color;
        bColor.a = 0.5f;

        a = shapeA.ScaledPoints(shapeA.polypoints);
        b = shapeB.ScaledPoints(shapeB.polypoints);
        //UpdateMinAvgMinDistance();
        UpdateHausdorffDistance();
    }
    #endregion

    #region Callbacks
    void Start()
    {
        Init();
    }

    // Update is called once per frame
    void Update()
    {
        if (a != shapeA.polypoints || b != shapeB.polypoints)
        {
            Init();
        }
    }

    private void OnDrawGizmos()
    {
        Vector2 yOffset = Vector2.down * 0.03f;

        if (shapeA && shapeB)
        {
            aClosest = shapeA.TranslatePoints(aClosest);
            bClosest = shapeB.TranslatePoints(bClosest);

            Gizmos.color = aColor;
            for (int i = 0; i < aClosest.Length; i++)
            {
                Gizmos.DrawLine(a[i], aClosest[i]);
#if UNITY_EDITOR
                UnityEditor.Handles.Label(a[i] + yOffset, string.Format("a{0}", i));
#endif
            }
            Gizmos.color = bColor;
            for (int i = 0; i < bClosest.Length; i++)
            {
                Gizmos.DrawLine(b[i], bClosest[i]);
#if UNITY_EDITOR
                UnityEditor.Handles.Label(b[i] + yOffset, string.Format("b{0}", i));
#endif
            }


        }
    }
    #endregion
}
