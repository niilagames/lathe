﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Handle : MonoBehaviour
{
    #region Declarations
    public bool isPressed = false;
    bool wasWithinBounds;
    float zPosition = 0;
    float subjectRadius;
    float subjectLength;
    Bounds bounds;
    [SerializeField] float minX, minY;
    [SerializeField] float maxX, maxY;
    public Vector3 mousePos;
    public Vector3 mousePosInWorldSpace;
    public SpriteRenderer sr;

    public CarvingSubjectMesh generator;

    #endregion

    #region Methods
    public void Initialize(Bounds _bounds)
    {
        sr = GetComponent<SpriteRenderer>();
        bounds = _bounds;
        minX = _bounds.center.x - _bounds.extents.x;
        maxX = _bounds.center.x + _bounds.extents.x;
        maxY = _bounds.center.y + _bounds.extents.y;
        minY = _bounds.center.y - _bounds.extents.y;
    }

    public bool IsWithinBounds()
    {
        return bounds.Contains(transform.position);
    }
    #endregion

    #region Callbacks
    void Start()
    {
        
    }

    

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            isPressed = false;
        }

        mousePos = Input.mousePosition;
        mousePos = new Vector3(mousePos.x, mousePos.y, -Camera.main.transform.position.z);
        mousePosInWorldSpace = Camera.main.ScreenToWorldPoint(mousePos);
        if (isPressed)
        {
            Vector3 objectPosition = mousePosInWorldSpace;
            objectPosition.x = Mathf.Clamp(objectPosition.x, minX, maxX);
            objectPosition.y = Mathf.Clamp(objectPosition.y, -Mathf.Infinity, maxY-0.01f);
            objectPosition.z = zPosition;

            transform.position = objectPosition;

            if (transform.hasChanged)
            {
                if (IsWithinBounds()) LineCarver.PositionChanged(this);

                if (!wasWithinBounds && IsWithinBounds())
                {
                    LineCarver.EnteredBounds(this);
                    wasWithinBounds = true;
                    
                }
                else if (wasWithinBounds && !IsWithinBounds())
                {
                    LineCarver.LeftBounds(this);
                    wasWithinBounds = false;
                    Debug.Log("left bounds");
                }
            }
        }
    }
    #endregion
}
