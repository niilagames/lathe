﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static BMesh;
using ClipperLib;
using System.Linq;

using Vector2i = ClipperLib.IntPoint;
using Polygon = System.Collections.Generic.List<ClipperLib.IntPoint>;

public class CarvingController : MonoBehaviour
{
    #region Declarations
    public List<Vector2> edge = new List<Vector2>();
    Polygon polygon = new Polygon();
    public List<Bounds> bounds;
    [Tooltip("The height to add to each bounds to increase chances of contact detection.")]
    public float heightTolerance = 0.1f;
    [Header("Shape options")]
    public Vector3 origin = Vector3.zero;
    [Tooltip("The minimum difference in angles between line segments to count as a change in height.")]
    public float minAngle = 5f;
    public float length;
    public float radius;
    [Min(2)]
    public int circleSegments;
    public CarvingSubjectMesh generator;
    public BMesh mesh;
    public MeshFilter mf;
    [Header("Debug options")]
    public bool displayEdge = false;
    public bool displayBounds = false;

    bool hasChanged = false;
    #endregion

    #region Methods
    void FitLengthToCamera()
    {
        Vector2 leftPos = Camera.main.WorldToViewportPoint(new Vector3(-length / 2, 0, -radius));
        if (leftPos.x < 0.05)
        {
            length -= 0.2f;
            FitLengthToCamera();
        }
    }

    public void UpdateMesh(List<Vector2> _edge, CarvingSubjectMesh target, bool forceUpdate = false)
    {
        Vector2[] _positions = edge.ToArray();
        if (_positions != target.positions || forceUpdate)
        {
            target.GenerateCylinderBasedOnPolyline(_positions, minAngle, radius, length, circleSegments, mesh, mf);
            target.positions = _positions;
        }
    }

    void ResetShape()
    {
        polygon.Clear();
        Vector2i p1 = new Vector2(-length / 2, -radius).ToVector2i();
        Vector2i p2 = new Vector2(length / 2, 0f).ToVector2i();
        polygon.Add(new Vector2i(p1.X, p2.Y));
        polygon.Add(new Vector2i(p1.X, p1.Y));
        polygon.Add(new Vector2i(p2.X, p1.Y));
        polygon.Add(new Vector2i(p2.X, p2.Y));

        ExtractEdgeFromPolygon();
    }

    void ExtractEdgeFromPolygon()
    {
        edge.Clear();
        if (polygon == null) return;

        Vector2 p;
        for (int i = 1; i < polygon.Count-1; i++)
        {
            p = polygon[i].ToVector2();
            p.y = -p.y;
            edge.Add(p);
        }
    }

    public void Clip(CuttingTool cutter)
    {
        //TODO: Add handling for if any Y positions equal zero = we've got two shapes. I think what should happen is that whichever bit is attached to the center gets the stay. The other one gets its own mesh generator (in which case we probably need to make it a class with init properties) and is removed with animation/coroutine/whatever.
        List<Polygon> result = new List<Polygon>();
        Clipper clipper = new Clipper();
        clipper.AddPolygon(polygon, PolyType.ptSubject);
        clipper.AddPolygon(cutter.clipPolygon, PolyType.ptClip);
        clipper.Execute(ClipType.ctDifference, result, PolyFillType.pftNonZero, PolyFillType.pftNonZero);
        if (result.Count <= 1) {
            polygon = result[0];
        }
        else
        {
            float area = 0;
            Polygon biggestPolygon = null ;
            // Using a for loop here is not ideal.
            for (int i = 0; i < result.Count; i++)
            {
                float currentArea = (float)Clipper.Area(result[i]);
                if (currentArea > area) {
                    area = currentArea;
                    biggestPolygon = result[i];
                }
            }
            polygon = biggestPolygon;
        }
        Clipper.CleanPolygon(polygon);

        ExtractEdgeFromPolygon();
        LatheEvents.OnShapeChanged();
    }

    public bool CutterWithinBounds(CuttingTool cutter)
    {

        for (int i = 0; i < bounds.Count; i++)
        {
            if (bounds[i].Intersects(cutter.bounds))
            {
                return true;
            }
        }

        return false;

    }

    public void GenerateBoundsShape(List<Vector2> edge)
    {
        // Generates a list of bounds that closely match the shape polygon. Assumes a progressively rising X value. If this isn't the case, we can always add a sort.
        // Could probably build this into either the meshgen or edgegen method if performance is important.
        UnityEngine.Profiling.Profiler.BeginSample("Generate Bounds Shape");
        bounds.Clear();

        Vector2 currentPoint = edge[0];
        Vector2 nextPoint;
        float startX = currentPoint.x;
        float endX;
        float maxY = currentPoint.y;
        float boundsHeight = maxY + heightTolerance;

        for (int i = 0; i < edge.Count-1; i++)
        {
            // this could potentially also be a while loop, ending when maxY != boundsHeight or we're at the last position.

            currentPoint = edge[i];
            nextPoint = edge[i+1];
            endX = currentPoint.x;
            maxY = Mathf.Max(currentPoint.y, nextPoint.y);

            if (i == edge.Count - 2) // I don't like doing it this way; see comment about while loop above.
            {
                Vector2 size = new Vector2(nextPoint.x - startX, boundsHeight);
                Vector2 center = new Vector2(startX + size.x / 2, origin.y - boundsHeight / 2);
                Bounds b = new Bounds(center, size);
                bounds.Add(b);
                continue;
            }

            if (maxY != boundsHeight) // If the highest point of this point pair has changed, we can build new bounds from this point on.
            {
                Vector2 size = new Vector2(endX - startX, boundsHeight);
                Vector2 center = new Vector2(startX + size.x/2, origin.y - boundsHeight / 2);
                Bounds b = new Bounds(center, size);
                bounds.Add(b);

                boundsHeight = maxY + heightTolerance;
                startX = endX;
            }
        }
        UnityEngine.Profiling.Profiler.EndSample();

    }
    #endregion

    #region Event Handlers
    void PostShapeChangeEvent()
    {
        GenerateBoundsShape(edge);
        UpdateMesh(edge, generator);
    }

    void OnEnter(LatheState state)
    {
        if (state is LathePreGameState)
        {
            Init();
        }
    }

    void OnExit(LatheState state)
    {
        if (state is LathePostGameState)
        {
            ResetShape();
            PostShapeChangeEvent();
        }
    }
    #endregion

    #region Callbacks
    private void OnEnable()
    {
        LatheEvents.OnShapeChanged += PostShapeChangeEvent;
        LatheEvents.OnStateEnter += OnEnter;
        LatheEvents.OnStateExit += OnExit;
    }

    private void OnDisable()
    {
        LatheEvents.OnShapeChanged -= PostShapeChangeEvent;
        LatheEvents.OnStateEnter -= OnEnter;
        LatheEvents.OnStateExit -= OnExit;
    }

    public void Init()
    {
        FitLengthToCamera();
        if (polygon == null || polygon.Count <= 2) // If we only have one edge or less, we want to reset the shape.
        {
            ResetShape();
        }
        else
        {
            ExtractEdgeFromPolygon();
        }
        PostShapeChangeEvent();

    }

    private void Awake() // remove this when implementing state machine.
    {
        //Init();
    }

    void Update()
    {
        if (hasChanged)
        {
            UpdateMesh(edge, generator);
        }

    }

    private void OnDrawGizmos()
    {
        if (polygon.Count > 0 && displayEdge)
        {
            for (int i = 0; i < polygon.Count - 1; i++)
            {
                Gizmos.color = Color.white;
                Vector3 p = polygon[i].ToVector3();
                Gizmos.DrawLine(p, polygon[i + 1].ToVector3());
                //Gizmos.DrawSphere(p, 0.02f);
            }
        }

        if (displayBounds)
        {
            for (int i = 0; i < bounds.Count; i++)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawWireCube(bounds[i].center, bounds[i].size);
            }

            for (int i = 0; i < edge.Count; i++)
            {
                Vector2 pos = edge[i];
                pos.y = -pos.y;
#if UNITY_EDITOR
                UnityEditor.Handles.Label(pos + Vector2.down * 0.1f, i.ToString());
#endif
            }
        }
    }
    #endregion
}
