﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LatheState
{
    // This could probably also be done with events.
    protected StateManager stateManager;
    protected CarvingController controller;
    protected LatheState nextState;

    protected LatheState(CarvingController controller, StateManager stateManager)
    {
        this.controller = controller;
        this.stateManager = stateManager;
    }

    public virtual void Enter()
    {
        
    }

    public virtual void PostEnter()
    {

    }

    public virtual void Exit()
    {
        nextState = null;
    }

    public virtual void EnterNextState()
    {
        if (nextState != null)
        {
            stateManager.ChangeState(nextState);
        }
    }

}

public class LathePreGameState : LatheState
{
    public LathePreGameState(CarvingController controller, StateManager stateManager) : base(controller, stateManager)
    {

    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void PostEnter()
    {
        base.PostEnter();
        stateManager.ChangeState(GameManager.Instance.playState);
    }

    public override void Exit()
    {
        base.Exit();
    }
}

public class LathePlayState : LatheState
{
    public LathePlayState(CarvingController controller, StateManager stateManager) : base(controller, stateManager)
    {

    }

    public override void Enter()
    {
        base.Enter();
        nextState = GameManager.Instance.evaluatingState; // If there's no blueprint, do a different state.
        controller.GetComponent<Spin>().spinning = true;
    }

    public override void Exit()
    {
        base.Exit();
    }
}

public class LathePostGameState : LatheState
{
    // Used for all states that appear right after turning. Used for displaying and/or evaluating the work.
    public LathePostGameState(CarvingController controller, StateManager stateManager) : base(controller, stateManager)
    {

    }

    public override void Enter()
    {
        base.Enter();
        nextState = GameManager.Instance.endingState;
        // Receive mesh from carvingcontroller, perform animations, etc.
        // Send event to UI
        //Debug.Log("Entered postgame state");

    }

    public override void Exit()
    {
        base.Exit();
    }
}

public class LatheEvaluatingState : LathePostGameState
{
    public LatheEvaluatingState(CarvingController controller, StateManager stateManager) : base(controller, stateManager)
    {

    }

    public override void Enter()
    {
        base.Enter();
        nextState = GameManager.Instance.playState;
    }

    /* What does this state need to know?
     * - Scoring bands - pull these from the game manager, which knows about settings.
     * - Accepted threshold - so far this isn't defined.
     * - blueprint - get this from game manager.
     * - vector2 edge from cylinder - get this from carvingcontroller
     * - stop spinning
     * - get mesh from carving controller for animation
     */
}

public class LatheEndingState : LatheState
{
    public LatheEndingState(CarvingController controller, StateManager stateManager) : base(controller, stateManager)
    {

    }

    public override void Enter()
    {
        base.Enter();
        //Debug.Log("Entered ending state");
    }
}