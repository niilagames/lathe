﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using static BMesh;

public class CarvingSubjectMesh : MonoBehaviour
{
    /*
     * Possible ideas for a more efficient meshgen algorithm (apart from just making it better):
     * If number of incoming points is equal to number of existing points, rather than generating an entire new mesh, we can just adjust the position of each vertex.
     * One step further: Can we use some sort of vector pooling? I think assigning stuff to memory each frame is taking up a lot of resources, so if there are less incoming points, we can just use the ones we have and discard the rest. I suspect we can do something similar with moe points - simply get each longedge, add points, draw faces. AddFaceStrip might have to accept slices of an array rather than the full array.
     * While mesh is actively being edited, lower the number of circle segments, then increase them again once editing has stopped.
     */
    // TODO: Clean up this mess. Probably need some properties.

    #region Declarations
    public Vector2[] positions;
    MeshRenderer mr;
    MeshFilter mf;
    public BMesh mesh;

    [Header("Gizmo options")]
    public bool drawVerts = false;
    public bool drawVertexNumbers = false;
    public bool drawBMesh = false;

    #endregion

    #region enums
    enum FaceDirection
    {
        Clockwise,
        Counterclockwise
    }
    #endregion

    #region Methods

    void AddCircleFan(BMesh _mesh, Vertex center, List<Vertex[]> edges, int faceIndex, int _circleSegments, float _radius, FaceDirection direction, bool copyEdgeLoop = true)
    {
        for (int i = 0; i < _circleSegments; i++)
        {
            Vertex v1 = center;
            center.attributes["uv"] = new FloatAttributeValue(0.5f, 0.5f);
            Vertex v2;
            Vertex v3;
            if (copyEdgeLoop)
            {
                v2 = _mesh.AddVertex(edges[i][faceIndex].point);
                v3 = _mesh.AddVertex(edges[i == edges.Count - 1 ? 0 : i + 1][faceIndex].point);
                // It feels sort of weird to have to pass in a radius here, but for now the purpose is to maintain UV positions even with a varying circle fan radius. At some point we should find a different solution - maybe set radius locally on init? Doesn't make much of a difference, I guess.
                v2.attributes["uv"] = new FloatAttributeValue(0.5f + (v2.point.x) / (2 * _radius), 0.5f + (v2.point.y) / (2 * _radius)); 
                v3.attributes["uv"] = new FloatAttributeValue(0.5f + (v3.point.x) / (2 * _radius), 0.5f + (v3.point.y) / (2 * _radius));
            }
            else
            {
                v2 = edges[i][faceIndex];
                v3 = edges[i == edges.Count - 1 ? 0 : i + 1][faceIndex];
            }

            if (direction == FaceDirection.Clockwise)
            {
                _mesh.AddFace(v1, v2, v3);
            }
            else
            {
                _mesh.AddFace(v1, v3, v2);
            }
        }
    }

    bool IsValidNewPoint(Vector2 p1, Vector2 p2, Vector2 p3, float _minAngle)
    {
        Vector2 prevDir = (p2 - p1).normalized;
        Vector2 dir = (p3 - p2).normalized;
        float angleBetweenDirections = Vector2.Angle(prevDir, dir);
        return angleBetweenDirections >= _minAngle;
    }

    Vector2[] SamplePolyLine(Vector2[] polyline, float _minAngle)
    {

        List<Vector2> points = new List<Vector2>();
        Vector2 samplePoint = polyline[0];
        points.Add(samplePoint);
        Vector2 lastSamplePoint;
        for (int i = 1; i < polyline.Length; i++)
        {
            lastSamplePoint = samplePoint;
            samplePoint = polyline[i];
            if (IsValidNewPoint(points[points.Count - 1], lastSamplePoint, samplePoint, _minAngle))
            {
                points.Add(lastSamplePoint);
            }
        }
        points.Add(polyline[polyline.Length - 1]);

        return points.ToArray();
    }

    //Vector2[] SampleCurve(AnimationCurve curve, int sampleRate, float radius, float length)
    //{
    //    // Currently samples any keyframes in curve even if sample count is low enough to not reach them.

    //    //Vector2 MapCurveToCoords(float curveTime, float curveValue)
    //    //{
    //    //    return new Vector2(curveTime * length, curveValue * radius);
    //    //}
        
    //    List<Vector2> points = new List<Vector2>();
    //    List<Keyframe> keys = curve.keys.ToList();
    //    keys.Reverse();

    //    Vector2 samplePoint = MapCurveToCoords(0, curve.Evaluate(0));
    //    float percentageOfLength = 0;
    //    points.Add(samplePoint);
    //    Vector2 lastSamplePoint;
    //    while (percentageOfLength < 1)
    //    {
    //        percentageOfLength += 1 / (float)sampleRate;
    //        lastSamplePoint = samplePoint;
    //        float curveValue = curve.Evaluate(percentageOfLength);
    //        samplePoint = MapCurveToCoords(percentageOfLength, curveValue);
    //        if (keys.Count > 0)
    //        {
    //            Keyframe nextKey = keys[keys.Count - 1];
    //            if (percentageOfLength >= nextKey.time)
    //                {
    //                    samplePoint = MapCurveToCoords(nextKey.time, nextKey.value);
    //                    keys.Remove(nextKey);
    //                    percentageOfLength = nextKey.time; // reset our place on the curve to where we found the keyframe.
    //                }
    //        }

    //        if (IsValidNewPoint(points[points.Count - 1], lastSamplePoint, samplePoint, _minAngle))
    //        {
    //            points.Add(lastSamplePoint);
    //        }
    //    }
    //    points.Add(MapCurveToCoords(1, curve.Evaluate(1)));

    //    return points.ToArray();
    //}

    /// <summary>
    /// Builds a segmented prismoid cylinder centered around the origin.
    /// </summary>
    /// <param name="points">An array of Vector2 positions that will be rotated along the z axis as vertices. x denotes how far to move along the z axis, y denotes the vertex distance from the axis.</param>
    /// <param name="origin">The center of the cylinder.</param>
    /// <param name="circleSegments">The number of vertices to place in each edge loop of the cylinder.</param>
    BMesh GenerateCylinder(Vector2[] points, Vector3 origin, float maxRadius, float maxLength, int _circleSegments)
    {
        BMesh _mesh = new BMesh();

        List<Vertex[]> longEdges = new List<Vertex[]>();
        Vector3 offset = origin - Vector3.back * maxLength/2;
        var uv = _mesh.AddVertexAttribute("uv", AttributeBaseType.Float, 2);

        //Temporary.
        float circumference = Mathf.PI * 2.0f * maxRadius;
        float uvScale = Mathf.Max(1, Mathf.Round(circumference)) / circumference;

        for (int i = 0; i <= _circleSegments; i++)
        {
            List<Vertex> edge = new List<Vertex>();
            for (int pidx = 0; pidx < points.Length; pidx++)
            {
                float angle = Mathf.Deg2Rad * (360 / (float)_circleSegments * i);
                float _radius = points[pidx].y;
                //if (_radius <= 0 || _radius > maxRadius)
                //{
                //    // A zero or negative radius means that we've cut through the subject. A radius bigger than the initial radius means that somehow the subject has grown in size. Whatever we want to do in this case, we do it here. For now, discard.
                //    Debug.Log("cut thru");
                //    continue;
                //}
                //if (points[pidx].x > offset.z || points[pidx].x > -offset.z)
                //{
                //    // An x value of less than 0 or more than length means that the subject gets longer than its initial length. Whatever should happen in this case is handled here. For now, discard.
                //    continue;
                //}
                Vector3 pointPos = new Vector3(Mathf.Cos(angle) * _radius, Mathf.Sin(angle) * _radius, points[pidx].x); // Rotate point around axis
                Vertex v = new Vertex(pointPos);
                _mesh.AddVertex(v); // add edge vertex to mesh.
                float distanceAlongEdge = Mathf.InverseLerp(-maxLength / 2, maxLength / 2, points[pidx].x);
                v.attributes["uv"] = new FloatAttributeValue(Mathf.Lerp(0, 1, distanceAlongEdge) * uvScale, Mathf.Lerp(0, 1, (float)i / _circleSegments) * uvScale);
                edge.Add(v); // store vertex in current edge. BMesh seems to have some edge info, so perhaps we are reinventing the wheel here.
            }
            longEdges.Add(edge.ToArray());
            if (i > 0) // On subsequent passes after the first, we can begin making face strips to fill out the cylinder.
            {
                int currentEdgeIndex = i;
                int previousEdgeIndex = i - 1;
                Vertex[] currentEdge = longEdges[currentEdgeIndex];
                Vertex[] previousEdge = longEdges[previousEdgeIndex];
                //AddFaceStrip(_mesh, currentEdge, previousEdge);
                _mesh.AddFaceStrip(currentEdge, previousEdge);
            }
        }

        // Add caps to cylinder. At a later time, we might need to change offset to a different variable to make sure that the end cap is always drawn at the outermost edge loops rather than always at each end.
        Vertex capVertex1 = _mesh.AddVertex(0, 0, points[0].x);
        Vertex capVertex2 = _mesh.AddVertex(0, 0, points[points.Length-1].x);

        AddCircleFan(_mesh, capVertex1, longEdges, 0, _circleSegments, maxRadius, FaceDirection.Clockwise);
        AddCircleFan(_mesh, capVertex2, longEdges, longEdges[0].Length-1, _circleSegments, maxRadius, FaceDirection.Counterclockwise);

        return _mesh;
    }

    //void GenerateCylinderBasedOnCurve()
    //{
    //    mf.mesh.Clear();
    //    Vector2[] edgePoints = SampleCurve(drawCurve, curveSamples, radius, length);
    //    mesh = GenerateCylinder(edgePoints, transform.position, circleSegments);
    //    BMeshUnity.SetInMeshFilter(mesh, mf);
    //}

    public void GenerateCylinderBasedOnPolyline(Vector2[] line, float _minAngle, float _radius, float _length, int _circleSegments, BMesh _mesh, MeshFilter mf)
    {
        mf.mesh.Clear();
        Vector2[] edgePoints = SamplePolyLine(line, _minAngle);
        mesh = GenerateCylinder(edgePoints, transform.position, _radius, _length, _circleSegments);
        BMeshUnity.SetInMeshFilter(mesh, mf);
    }

    #endregion

    #region Callbacks

    private void OnDrawGizmos()
    {
        if (mesh != null)
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            if (drawBMesh) BMeshUnity.DrawGizmos(mesh);
            if (drawVerts || drawVertexNumbers)
            {
                for (int i = 0; i < mesh.vertices.Count; i++)
                {
                    Vertex vert = mesh.vertices[i];
                    Gizmos.color = Color.gray;
                    if (drawVerts) Gizmos.DrawSphere(vert.point, 0.06f);
#if UNITY_EDITOR
                    if (drawVertexNumbers) UnityEditor.Handles.Label(vert.point + mf.mesh.normals[i] * 0.5f, i.ToString());
#endif
                }
            }
        }
    }
    #endregion
}
