﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickToDrag : MonoBehaviour
{
    #region Declarations
    public CuttingTool tool;
    [Min(0)]
    public float normalMaxSpeed = 1;
    [Min(0)]
    public float contactingMaxSpeed = 0.8f;
    float DragSpeed
    {
        // Currently in absolute units. For ease of use, consider setting contacting max speed to a fraction of normal drag speed.
        get
        {
            if (tool.contacting)
            {
                return contactingMaxSpeed;
            }
            else
            {
                return normalMaxSpeed;
            }
        }
    }
    [Tooltip("The minimum drag distance before the tool moves.")]
    public float minimumDelta = 0.05f;
    float minimumDeltaSquared;
    bool beingDragged;
    float maxPositionY;
    Vector3 previousPosition;
    Camera mainCamera;
    #endregion

    #region Methods
    void Move(Vector3 direction, float maxDistance)
    {
        direction = direction.normalized * Mathf.Clamp(direction.magnitude, 0, maxDistance);
        Vector3 pos = transform.position + direction;
        if (pos.y > maxPositionY)
        {
            pos.y = maxPositionY;
        }
        transform.position = pos;
    }
    #endregion

    #region Callbacks
    private void OnMouseDown()
    {
        beingDragged = true;
        previousPosition = mainCamera.MousePosition();
    }
    private void OnMouseUp()
    {
        beingDragged = false;
    }
    private void Start()
    {
        minimumDeltaSquared = minimumDelta * minimumDelta;

        if (!mainCamera) mainCamera = Camera.main;

        if (tool == null)
        {
            tool = GetComponent<CuttingTool>();
        }
        maxPositionY = tool.controller.transform.position.y - tool.bounds.size.y / 2 + 0.01f;
    }

    private void Update()
    {
        Vector3 mousePos = mainCamera.MousePosition();
        Vector3 dir = mousePos - previousPosition;

        if (beingDragged && dir.sqrMagnitude >= minimumDeltaSquared)
        {
            Move(dir, DragSpeed);
            previousPosition = mousePos;
        }
    }
    #endregion
}
