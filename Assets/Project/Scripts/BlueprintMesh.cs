﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static BMesh;

public class BlueprintMesh : MonoBehaviour
{
    #region Declarations
    public Blueprint shape;
    private MeshFilter mf;
    private BMesh bmesh;
    private Mesh mesh;
    #endregion

    #region Methods
    void BuildMesh()
    {
        BMesh _bmesh = shape.MeshFromShape(shape);
        BMeshUnity.SetInMeshFilter(_bmesh, mf);
    }

    void UpdateBlueprint()
    {
        shape = GameManager.Instance.blueprintQueue.CurrentBlueprint;
    }
    public void Init()
    {
        mf = GetComponent<MeshFilter>();
        UpdateBlueprint();
        if (shape != null) // Add error / messaging systtem here.
        {
            BuildMesh();
            mesh = new Mesh();
            mesh.MarkDynamic();
        }
    }

    public void Clear()
    {
        mf.mesh = null;
    }
    #endregion

    #region Event Handlers

    void OnPostEnter(LatheState state)
    {
        if (state is LathePlayState) // Probably change this to an inspector value so we can use blueprint mesh in every state we want.
        {
            Init();
        }
    }

    void OnExit(LatheState state)
    {
        if (state is LathePlayState)
        {
            Clear();
        }
    }

    #endregion

    #region Callbacks

    void Start()
    {
        
    }

    private void OnValidate()
    {
        //if (mf == null) mf = GetComponent<MeshFilter>();
        //if (mesh == null) mesh = new Mesh();
        //mesh.MarkDynamic();

        //BuildMesh();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        LatheEvents.OnStatePostEnter += OnPostEnter;
        LatheEvents.OnStateExit += OnExit;
    }

    private void OnDisable()
    {
        LatheEvents.OnStatePostEnter -= OnPostEnter;
        LatheEvents.OnStateExit -= OnExit;
    }
    #endregion
}
