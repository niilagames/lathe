﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ClipperLib;
using System.Linq;
using UnityEditor;
using Sirenix.OdinInspector;

using Vector2i = ClipperLib.IntPoint;
using Polygon = System.Collections.Generic.List<ClipperLib.IntPoint>;
using Polygons = System.Collections.Generic.List<System.Collections.Generic.List<ClipperLib.IntPoint>>;

[ExecuteInEditMode]
public class ScoreThresholdDisplay : MonoBehaviour
{
    // Updating of display is being called in several different ways right now; no ideal. TODO: Streamline. Or not, this is just a display script.
    #region Declarations
    public bool showThresholdGizmos;
    [ShowIfGroup("Display options", Condition = nameof(showThresholdGizmos))]
    public Blueprint previewShape;
    [ShowIfGroup("Display options", Condition = nameof(showThresholdGizmos))]
    public Polygon shape;
    [ShowIfGroup("Display options", Condition = nameof(showThresholdGizmos))]
    public Gradient displayGradient;
    public ScoreThresholdSettings settings;

    Blueprint ActiveBlueprint { get { return activeBlueprint; } set { activeBlueprint = value; UpdateDisplay(activeBlueprint.points); } }
    Blueprint activeBlueprint;
    
    Polygons[] polygonsPos;
    Polygons[] polygonsNeg;
    #endregion

    

    #region Methods
    void BuildDistanceField()
    {
        Polygons subject = new Polygons { shape };
        polygonsPos = new Polygons[settings.limits.Length];
        polygonsNeg = new Polygons[settings.limits.Length];

        float distanceCumulative = 0;

        for (int i = 0; i < settings.limits.Length; i++)
        {
            distanceCumulative += settings.limits[i].distance;
            polygonsPos[i] = Clipper.OffsetPolygons(subject, distanceCumulative * MathExtension.CliperLibRatio, ClipperLib.JoinType.jtRound);
            polygonsNeg[i] = Clipper.OffsetPolygons(subject, -distanceCumulative * MathExtension.CliperLibRatio, ClipperLib.JoinType.jtRound);
        }
    }

    Polygon BuildPolygon()
    {
        return BuildPolygon(previewShape.points);
    }

    Polygon BuildPolygon(Vector2[] input)
    {
        Vector2i[] points = new Vector2i[input.Length * 2];
        for (int i = 0, j = points.Length - 1; i < input.Length; i++, j--)
        {
            Vector2 p = input[i];
            points[i] = p.ToVector2i();
            p.y = -p.y;
            points[j] = p.ToVector2i();
        }
        return points.ToList();
    }

    public void Init()
    {
        UpdateDisplay();
    }

    public void Init(Vector2[] points)
    {
        UpdateDisplay(points);
    }

    public void UpdateDisplay()
    {
        if (previewShape)
        {
            shape = BuildPolygon();
            BuildDistanceField();
        }
    }

    public void UpdateDisplay(Vector2[] points)
    {
        shape = BuildPolygon(points);
        BuildDistanceField();
    }

    #endregion

    #region Callbacks
    private void OnEnable()
    {
        LatheEvents.ThresholdSettingsChanged += UpdateDisplay;
    }

    private void OnDisable()
    {
        LatheEvents.ThresholdSettingsChanged -= UpdateDisplay;
    }

    private void Start()
    {
        // Some of this should probably happen inside init function.
        Blueprint currentBlueprint = null;
        if (GameManager.Instance != null) currentBlueprint = GameManager.Instance.blueprintQueue.CurrentBlueprint;
        if (currentBlueprint != null)
        {
            Init(currentBlueprint.points);
        }
        else
        {
            Init();
        }
    }
    private void OnApplicationQuit()
    {
        ActiveBlueprint = previewShape;
    }

    void OnValidate()
    {
        UpdateDisplay();
    }

    private void OnDrawGizmos()
    {
        if (!Application.isPlaying && ActiveBlueprint != previewShape)
        {
            //ActiveBlueprint = previewShape;
        }

        if (showThresholdGizmos && previewShape != null && shape.Count > 1)
        {
            Vector3 offsetPos = transform.position;

            for (int i = 0; i < shape.Count; i++) // Draw blueprint outline
            {
                Gizmos.color = Color.white;
                Gizmos.DrawLine(offsetPos + shape[i].ToVector3(), offsetPos + shape[(i + 1) % shape.Count].ToVector3());
            }

            for (int i = 0; i < polygonsPos.Length; i++) // Draw scoring bands
            {
                //Gizmos.color = Color.Lerp(Color.yellow, Color.red, (float)i / polygonsPos.Length); // Set color of this band
                Gizmos.color = displayGradient.Evaluate((float)i / polygonsPos.Length); // Set color of this band
                for (int j = 0; j < polygonsPos[i].Count; j++)
                {
                    for (int k = 0; k < polygonsPos[i][j].Count; k++)
                    {
                        Gizmos.DrawLine(offsetPos + polygonsPos[i][j][k].ToVector3(), offsetPos + polygonsPos[i][j][(k + 1) % polygonsPos[i][j].Count].ToVector3());
                    }
                }
                for (int j = 0; j < polygonsNeg[i].Count; j++)
                {
                    for (int k = 0; k < polygonsNeg[i][j].Count; k++)
                    {
                        Gizmos.DrawLine(offsetPos + polygonsNeg[i][j][k].ToVector3(), offsetPos + polygonsNeg[i][j][(k + 1) % polygonsNeg[i][j].Count].ToVector3());
                    }
                }
            }
        }
    }
    #endregion
}
