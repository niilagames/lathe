﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ScoreThreshold // TODO: Rename to something like Limit?
{
    public ScoreThreshold(float _distance, float _value) { distance = _distance; value = _value; }
    [Min(0.05f)] // Distance can't be 0 or negative. 0.05 is an arbitrary positive value.
    public float distance;
    public float value;
}
