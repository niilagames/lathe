﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveMesh : MonoBehaviour
{
    #region Declarations
    public GameObject template; // The prefab to instantiate in world space with the new mesh.
    #endregion

    GameObject CreatePrefabWithMesh(Mesh mesh)
    {
        // Either instantiate or save as asset. I think saving as asset might be smarter... Maybe check this link: https://forum.unity.com/threads/saving-modified-mesh.404212/
        return new GameObject();
    }
}
