﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchTools : MonoBehaviour
{
    #region Declarations
    public GameObject activeTool;
    public GameObject[] tools;
    [Min(0)]
    public int startingToolIndex;
    public Transform toolSpawn;
    Vector3 toolSpawnPosition
    {
        get
        {
            if (toolSpawn)
            {
                return toolSpawn.position;
            }
            else
            {
                Debug.LogWarning("No transform for tool spawn position is set. Tool will be spawned at Vector3.zero.", this);
                return Vector3.zero;
            }
        }
    }
    #endregion

    #region Event Handlers
    void OnPostEnter(LatheState state)
    {
        if (state is LathePlayState)
        {
            SetTool(startingToolIndex);
        }
    }

    void OnExit(LatheState state)
    {
        if (state is LathePlayState)
        {
            Clear();
        }
    }
    #endregion

    #region Methods
    public void SetTool(int newToolIndex)
    {
        if (tools.Length < 1)
        {
            Debug.LogError("Error: Tool list must contain at least one tool.", this);
            return;
        }
        Vector3 activeToolPosition = toolSpawnPosition;
        // Destroy current tool
        if (activeTool)
        {
            Debug.Log("hehe");
            activeToolPosition = activeTool.transform.localPosition;
            Destroy(activeTool);
        }
        // Get new tool prefab from tool list
        GameObject newToolPrefab = tools[newToolIndex >= 0 && newToolIndex < tools.Length ? newToolIndex : 0]; // Return first element of tool list if for some reason there's a bad index. Should maybe be turned into a bool method.
        // Instantiate prefab at prev tool position
        activeTool = Instantiate(newToolPrefab, activeToolPosition, Camera.main.transform.rotation, toolSpawn.parent);
    }

    public void Init()
    {
        SetTool(startingToolIndex);
    }

    public void Clear()
    {
        Destroy(activeTool);
    }
    #endregion

    #region Callbacks
    private void OnEnable()
    {
        LatheEvents.OnStateEnter += OnPostEnter;
        LatheEvents.OnStateExit += OnExit;
    }

    private void OnDisable()
    {
        LatheEvents.OnStateEnter -= OnPostEnter;
        LatheEvents.OnStateExit -= OnExit;

    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion
}
