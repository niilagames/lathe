﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class ExtensionMethods
{
    public static void Swap<T>(this List<T> list, int index1, int index2)
    {
        T temp = list[index1];
        list[index1] = list[index2];
        list[index2] = temp;
    }

    public static bool IsEmpty<T>(this List<T> list)
    {
        return list.Count == 0;
    }

    public static void AddFaceStrip(this BMesh bmesh, BMesh.Vertex[] edge1, BMesh.Vertex[] edge2)
    {
        int edgeResolution = edge1.Length;
        for (int vGroupIndex = 0; vGroupIndex < edgeResolution - 1; vGroupIndex++)
        {
            bmesh.AddFace(edge2[vGroupIndex], edge2[vGroupIndex + 1], edge1[vGroupIndex + 1], edge1[vGroupIndex]);
        }
    }

    /// <summary>
    /// Returns a boolean denoting whether a given point lies within the bounds delimited by two other points. This can be useful for determining whether a point on a line lies within a certain line segment.
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="point">The point for which to check intersection with bounds.</param>
    /// <returns></returns>
    public static bool WithinBounds(this Vector2 point, Vector2 a, Vector2 b)
    {
        float minX = Mathf.Min(a.x, b.x);
        float maxX = Mathf.Max(a.x, b.x);
        float minY = Mathf.Min(a.y, b.y);
        float maxY = Mathf.Max(a.y, b.y);

        return (point.x >= minX && point.x <= maxX && point.y >= minY && point.y <= maxY);
    }

    public static Vector3 MousePosition(this Camera cam)
    {
        return cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -cam.transform.position.z));
    }
}
