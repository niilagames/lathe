﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour
{
    #region Declarations
    public float rotationSpeedInDegrees;
    public bool spinning = true;
    #endregion

    #region Methods
    
    #endregion

    #region Callbacks
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 0, rotationSpeedInDegrees * Time.deltaTime));
    }
    #endregion
}
