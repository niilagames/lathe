﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Accuracy
{
    // TODO: Currently Hausdorff distance scales along with polygons. This is to be expected as it's simply an absolute distance between coordinate points, but it might be worth looking into adding some sort of normalization method.

    #region Structs and Enums
    // TODO: Potentially remove later.
    public struct PositionValuePair
    {
        public PositionValuePair(Vector2 pos, float dist)
        {
            Position = pos;
            Distance = dist;
        }

        public Vector2 Position { get; }
        public float Distance { get; }
    }

    public enum PolygonType
    {
        Open,
        Closed
    }
    #endregion

    #region Methods

    public static float DirectedWeightedMinDistance(Vector2[] polygonA, Vector2[] polygonB, float maxValue, out Vector2[] outList, PolygonType polyType = PolygonType.Closed)
    {
        PositionValuePair[] points = ClosestPoints(polygonA, polygonB, out outList, polyType);
        float accuracy = 0;
        float weight = 0;
        for (int i = 0; i < points.Length; i++)
        {
            float distance = points[i].Distance;
            float _weight = distance / maxValue;
            float _accuracy = distance * _weight;
            accuracy += _accuracy;
            weight += _weight;
        }

        return accuracy / weight;
    }

    static float DirectedAvgSquareMinDistance(Vector2[] polygonA, Vector2[] polygonB, out Vector2[] outList, PolygonType polyType = PolygonType.Closed)
    {
        PositionValuePair[] points = ClosestPoints(polygonA, polygonB, out outList, polyType);
        return points.Select(point => point.Distance * point.Distance).ToList().Average();
    }

    static float DirectedSummedMinDistance(Vector2[] polygonA, Vector2[] polygonB, out Vector2[] outList, PolygonType polyType = PolygonType.Closed)
    {
        PositionValuePair[] points = ClosestPoints(polygonA, polygonB, out outList, polyType);
        return points.Select(point => point.Distance * point.Distance).ToList().Sum();
    }

    static float DirectedMedianMinDistance(Vector2[] polygonA, Vector2[] polygonB, out Vector2[] outList, PolygonType polyType = PolygonType.Closed)
    {
        PositionValuePair[] points = ClosestPoints(polygonA, polygonB, out outList, polyType);
        List<float> distances = points.Select(point => point.Distance).OrderBy(point => point).ToList();
        float median;
        int halfIndex = distances.Count / 2;
        if (distances.Count % 2 == 0)
        {
            median = (distances[halfIndex] + distances[halfIndex - 1]) / 2;
        }
        else
        {
            median = distances[halfIndex];
        }
        return median;
    }

    public static float DirectedAvgMinDistance(Vector2[] polygonA, Vector2[] polygonB, out Vector2[] outList, PolygonType polyType = PolygonType.Closed)
    {
        PositionValuePair[] points = ClosestPoints(polygonA, polygonB, out outList, polyType);
        return points.Select(point => point.Distance).ToList().Average();
    }

    /// <summary>
    /// Returns the point on a polygon that is closest to a given source point as well as the distance between the source and destination point.
    /// </summary>
    /// <param name="sourcePoint">The point from which to find the closest point on the polygon.</param>
    /// <param name="polygon">The set of points that make up the polygon. Points are assumed to be sequential.</param>
    /// <param name="polyType">Whether the points of the polygon make up a closed polygon or an open series of edges.</param>
    /// <returns></returns>
    public static PositionValuePair ClosestPointOnPolygon(Vector2 sourcePoint, Vector2[] polygon, PolygonType polyType = PolygonType.Closed)
    {
        // This algorithm works on the principle that the shortest distance between a point and a polygon will always be between either a vertex of the polygon or the foot of a line perpendicular to an edge of the polygon.
        int loopDuration;
        if (polyType == PolygonType.Open) // We can't loop all the way around if we're sampling an open edge rather than a closed polygon.
        {
            loopDuration = polygon.Length - 1;
        }
        else
        {
            loopDuration = polygon.Length;
        }

        List<PositionValuePair> points = new List<PositionValuePair>();

        for (int vi = 0; vi < loopDuration; vi++)
        {
            Vector3 p1 = polygon[vi];
            Vector3 p2 = polygon[(vi + 1) % polygon.Length]; // Always returns next point, even if end of array is reached
            Vector2 currentLineSegment = p2 - p1;
            Vector2 p3 = Vector3.Project((Vector3)sourcePoint - p1, currentLineSegment) + p1; // Draws a line through source point perpendicular to p1-p2 and returns the intersection point of these two lines.
            float distanceToP1 = Vector2.Distance(sourcePoint, p1);
            points.Add(new PositionValuePair(p1, distanceToP1));
            float distanceToP2 = Vector2.Distance(sourcePoint, p2);
            points.Add(new PositionValuePair(p2, distanceToP2));
            if (p3.WithinBounds(p1, p2)) // If p3 lies within the line segment delimited by p1 and p2
            {
                float distanceToP3 = Vector2.Distance(sourcePoint, p3);
                points.Add(new PositionValuePair(p3, distanceToP3));
            }
        }
        // Get point with shortest distance between origin and destination
        PositionValuePair closestPoint = points.Aggregate((p1, p2) => p1.Distance < p2.Distance ? p1 : p2);
        return closestPoint;
    }

    public static PositionValuePair[] ClosestPoints(Vector2[] polygonA, Vector2[] polygonB, out Vector2[] outList, PolygonType polyType = PolygonType.Closed)
    {
        PositionValuePair[] points = new PositionValuePair[polygonA.Length];
        outList = new Vector2[polygonA.Length];
        for (int ai = 0; ai < polygonA.Length; ai++)
        {
            Vector2 a = polygonA[ai];
            points[ai] = ClosestPointOnPolygon(a, polygonB, polyType);
            outList[ai] = points[ai].Position;
        }
        return points;
    }

    /// <summary>
    /// The largest guaranteed distance between any vertex in polygon A and any point in polygon B.
    /// </summary>
    /// <param name="polygonA"></param>
    /// <param name="polygonB"></param>
    /// <param name="polyType">Whether Polygon B is an open or closed polygon. In an open polygon, the edge between the first and last point of polygon B will not be counted as an edge for calculation purposes.</param>
    /// <returns></returns>
    public static float DirectedHausdorffDistance(Vector2[] polygonA, Vector2[] polygonB, PolygonType polyType = PolygonType.Closed)
    {
        Vector2[] temp;
        return DirectedHausdorffDistance(polygonA, polygonB, out temp, polyType);
    }
    /// <summary>
    /// The largest guaranteed distance between any vertex in polygon A and any point in polygon B.
    /// </summary>
    /// <param name="polygonA"></param>
    /// <param name="polygonB"></param>
    /// <param name="outList">A Vector2 array in which to place the positions of every point on polygon B that is the closest point of a vertex in polygon A.</param>
    /// <param name="polyType">Whether Polygon B is an open or closed polygon. In an open polygon, the edge between the first and last point of polygon B will not be counted as an edge for calculation purposes.</param>
    /// <returns></returns>
    public static float DirectedHausdorffDistance(Vector2[] polygonA, Vector2[] polygonB, out Vector2[] outList, PolygonType polyType = PolygonType.Closed)
    {
        // Extremely inefficient right now. Gets the shortest distance from each vertex in shape A to any point on shape B and then returns the LARGEST of these values. For each vertex i in shape A, the point at the shortest distance is placed into outList[i] for visualization and debugging purposes.

        // Bruteforce algorithm. Don't use this in production.
        PositionValuePair[] closestPoints = ClosestPoints(polygonA, polygonB, out outList, polyType);
        float maximinDistance = closestPoints.Max(point => point.Distance);
        return maximinDistance;
    }

    public static float ModifiedHausdorffDistance(Vector2[] a, Vector2[] b, out Vector2[] aDestinations, out Vector2[] bDestinations, PolygonType polyType = PolygonType.Closed)
    {
        return Mathf.Max(DirectedAvgMinDistance(a, b, out aDestinations, polyType), DirectedAvgMinDistance(b, a, out bDestinations, polyType));

    }

    public static float HausdorffDistance(Vector2[] a, Vector2[] b, PolygonType polyType = PolygonType.Closed)
    {
        return Mathf.Max(DirectedHausdorffDistance(a, b, polyType), DirectedHausdorffDistance(b, a, polyType));

    }

    /// <summary>
    /// The largest guaranteed distance between two polygons A and B.
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="aDestinations">The array in which to place the points on polygon B that are the closest points of polygon A's vertices.</param>
    /// <param name="bDestinations">The array in which to place the points on polygon A that are the closest points of polygon B's vertices.</param>
    /// <param name="polyType">Whether polygons are open or closed polygons. In an open polygon, the edge between the first and last point of the polygon will not be counted as an edge for calculation purposes.</param>
    /// <returns></returns>
    public static float HausdorffDistance(Vector2[] a, Vector2[] b, out Vector2[] aDestinations, out Vector2[] bDestinations, PolygonType polyType = PolygonType.Closed)
    {
        return Mathf.Max(DirectedHausdorffDistance(a, b, out aDestinations, polyType), DirectedHausdorffDistance(b, a, out bDestinations, polyType));
    }

    public static float AvgMinDistance(Vector2[] a, Vector2[] b, out Vector2[] aDestinations, out Vector2[] bDestinations, PolygonType polyType = PolygonType.Closed)
    {
        return (DirectedSummedMinDistance(a, b, out aDestinations, polyType) + DirectedSummedMinDistance(b, a, out bDestinations, polyType)) / (a.Length + b.Length);
    }

    public static float AvgSquareMinDistance(Vector2[] a, Vector2[] b, out Vector2[] aDestinations, out Vector2[] bDestinations, PolygonType polyType = PolygonType.Closed)
    {
        return (DirectedAvgSquareMinDistance(a, b, out aDestinations, polyType) + DirectedAvgSquareMinDistance(b, a, out bDestinations, polyType)) / 2;
    }

    public static float WeightedMinDistance(Vector2[] a, Vector2[] b, float maxValue, out Vector2[] aDestinations, out Vector2[] bDestinations, PolygonType polyType = PolygonType.Closed)
    {
        return (DirectedWeightedMinDistance(a, b, maxValue, out aDestinations, polyType) + DirectedWeightedMinDistance(b, a, maxValue, out bDestinations, polyType)) / 2;
    }

    public static float MedianMinDistance(Vector2[] a, Vector2[] b, out Vector2[] aDestinations, out Vector2[] bDestinations, PolygonType polyType = PolygonType.Closed)
    {
        return (DirectedMedianMinDistance(a, b, out aDestinations, polyType) + DirectedMedianMinDistance(b, a, out bDestinations, polyType)) / 2;
    }
    #endregion
}
