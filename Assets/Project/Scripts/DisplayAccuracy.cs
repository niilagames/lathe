﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayAccuracy : MonoBehaviour
{
    #region Declarations
    public EvaluateAccuracy evaluator;
    public string accText = "Current score: {0} %";
    public Text display;
    #endregion

    #region Methods
    void UpdateText()
    {
        //display.text = string.Format(accText, Mathf.Clamp((evaluator.relativeAccuracy * 100), 0f, 100f).ToString("F0"));
        display.text = string.Format(accText, evaluator.score.ToString("F2"));
    }
    #endregion

    #region Callbacks
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateText();
    }
    #endregion
}
