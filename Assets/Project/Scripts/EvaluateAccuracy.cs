﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EvaluateAccuracy : MonoBehaviour
{
    // Temporary script for testing accuracy evaluation between work piece and blueprint. TODO: Clean up. This is a mess.
    #region Declarations
    public Blueprint blueprint;
    public Vector2[] blueprintPolygon;
    public CarvingController controller;
    public Vector2[] blueprintDestinations;
    public Vector2[] workShapeDestinations;
    public float accuracy;
    public ScoreThresholdSettings scoring;
    public float score;
    Vector2 origin;
    #endregion

    #region Methods
    void UpdateBlueprint()
    {
        blueprint = GameManager.Instance.blueprintQueue.CurrentBlueprint;
        if (blueprint != null)
        {
            blueprintPolygon = blueprint.TransformedPoints;
        }
    }
    void Evaluate()
    {
        accuracy = Accuracy.ModifiedHausdorffDistance(blueprintPolygon, controller.edge.ToArray(), out blueprintDestinations, out workShapeDestinations, Accuracy.PolygonType.Open);
        score = scoring.CurrentBand(accuracy).MaxValue();
        Debug.Log(score);
        if (blueprint != null && score >= scoring.acceptedScore)
        {
            Debug.Log("That's a win");
            GameManager.Instance.blueprintQueue.RemoveOldest();
        }
        //Debug.Log(accuracy);
    }

    void Clear()
    {
        blueprint = null;
        controller = null;
        blueprintDestinations = null;
        workShapeDestinations = null;
        accuracy = 0;
        score = 0;
    }
    #endregion

    #region Event Handlers

    void OnPostEnter(LatheState state)
    {
        if (state is LatheEvaluatingState)
        {
            Init();
            Evaluate();
        }
    }

    void OnExit(LatheState state)
    {
        Clear();
    }
    #endregion

    #region Callbacks
    private void OnEnable()
    {
        //LatheEvents.OnShapeChanged += Evaluate;
        LatheEvents.OnStatePostEnter += OnPostEnter;
        LatheEvents.OnStateExit += OnExit;
    }
    private void OnDisable()
    {
        //LatheEvents.OnShapeChanged -= Evaluate;
        LatheEvents.OnStatePostEnter -= OnPostEnter;
        LatheEvents.OnStateExit -= OnExit;
    }

    public void Init()
    {
        controller = GameManager.Instance.carvingController;
        scoring = GameManager.Instance.scoreThresholdSettings;
        scoring.BuildBands();
        UpdateBlueprint();
        //if (blueprint != null) Evaluate();
    }
    
    void Start()
    {
        //Init();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        if (blueprint && controller && blueprintDestinations.Length > 0)
        {
            Gizmos.color = Color.red;
            for (int i = 0; i < blueprintPolygon.Length; i++)
            {
                Gizmos.DrawLine(blueprintPolygon[i], blueprintDestinations[i]);
            }

            Gizmos.color = Color.blue;
            for (int i = 0; i < controller.edge.Count; i++)
            {
                Gizmos.DrawLine(controller.edge[i], workShapeDestinations[i]);
            }
        }
    }
    #endregion
}
