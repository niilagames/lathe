﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controls : MonoBehaviour
{
    #region Declarations
    public CarvingController controller;
    public float angleThreshold;
    public int circleSegments;
    public float angleStep = 0.5f;
    public Text angleText;
    public Text segmentsText;
    public string angleDisplay = "Angle threshold: {0}";
    public string segmentsDisplay = "Cylinder segments: {0}";
    #endregion

    #region Methods
    public void IncreaseAngleThreshold()
    {
        angleThreshold = Mathf.Clamp(angleThreshold + angleStep, 0, 360);
        controller.minAngle = angleThreshold;
        controller.UpdateMesh(controller.edge, controller.generator, true);
        UpdateAngleText();
    }
    public void DecreaseAngleThreshold()
    {
        angleThreshold = Mathf.Clamp(angleThreshold - angleStep, 0, 360);
        controller.minAngle = angleThreshold;
        controller.UpdateMesh(controller.edge, controller.generator, true);
        UpdateAngleText();
    }

    void UpdateAngleText()
    {
        angleText.text = string.Format(angleDisplay, angleThreshold);
    }

    public void IncreaseCircleSegments()
    {
        circleSegments = Mathf.Clamp(circleSegments + 1, 2, 1000);
        controller.circleSegments = circleSegments;
        controller.UpdateMesh(controller.edge, controller.generator, true);
        UpdateSegmentsText();
    }
    public void DecreaseCircleSegments()
    {
        circleSegments = Mathf.Clamp(circleSegments - 1, 2, 1000);
        controller.circleSegments = circleSegments;
        controller.UpdateMesh(controller.edge, controller.generator, true);
        UpdateSegmentsText();
    }

    void UpdateSegmentsText()
    {
        segmentsText.text = string.Format(segmentsDisplay, circleSegments);
    }
    #endregion

    #region Callbacks
    void Start()
    {
        angleThreshold = controller.minAngle;
        circleSegments = controller.circleSegments;
        UpdateAngleText();
        UpdateSegmentsText();
    }

    // Update is called once per frame
    void Update()
    {

    }
    #endregion
}
