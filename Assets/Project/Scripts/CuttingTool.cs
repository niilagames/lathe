﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

using Vector2i = ClipperLib.IntPoint;
using Polygon = System.Collections.Generic.List<ClipperLib.IntPoint>;

public class CuttingTool : MonoBehaviour
{
    #region Declarations
    public Polygon clipPolygon = new Polygon();
    public Vector2[] toolShape;
    PolygonCollider2D poly;
    public CarvingController controller;
    public ParticleSystem ps;
    public Bounds bounds;
    public Vector2 previousPosition;
    //public LineRenderer lr;
    [Tooltip("The minimum distance between current and previous position to count as having moved.")]
    public float minimumDelta = 0.0001f;
    public bool contacting = false;
    bool hasMoved = false;
    #endregion

    #region Methods
    /// <summary>
    /// Forces contacting to be false if the tool stops moving for more than the specified duration.
    /// </summary>
    /// <param name="maxTime">The number of seconds to wait before setting contacting to false.</param>
    /// <returns></returns>
    IEnumerator StopContactingWhenNotMoving(float maxTime)
    {
        float time = 0;
        while (true)
        {
            if (hasMoved)
            {
                time = 0;
            }

            time += Time.deltaTime;
            if (time >= maxTime)
            {
                if (contacting)
                {
                    EndContact();
                }
            }

            yield return null;
        }
    }

    bool HasMoved(Vector2 pos1, Vector2 pos2, float minDistance)
    {
        // Currently not in use, we're using a janky little system that checks for transform.hasMoved every frame which seems to work without having to perform vector distance calculations - and potentially also covers changes in scale and rotation.
        float delta = Vector2.SqrMagnitude(pos1 - pos2);
        return (delta > minDistance);
    }
    
    void BeginContact()
    {
        contacting = true;
        ps.Play();
    }
    
    void EndContact()
    {
        contacting = false;
        ps.Stop();
    }

    void BuildBounds()
    {
        bounds = poly.bounds;
        bounds.center = transform.position;
    }

    void BuildBounds(Vector3 pos, Vector3 prevPos)
    {
        Vector3 diff = pos - prevPos;
        diff.x = Mathf.Abs(diff.x);
        diff.y = Mathf.Abs(diff.y);
        Vector3 centroid = (pos + prevPos) / 2;
        Vector3 size = poly.bounds.size + diff;
        bounds.center = centroid;
        bounds.size = size;
    }

    void SetToolShape(Vector2[] points)
    {
        // Remove duplicate coordinates from shape.
        points = (
            from p in points
            orderby p.x, p.y descending
            group p by p into g
            select g.First()
            ).ToArray();
        toolShape = points;
    }

    /// <summary>
    /// Builds a convex hull around two polygons located at the object's position last frame and this frame.
    /// </summary>
    /// <param name="currentPosition"></param>
    /// <param name="previousPosition"></param>
    void BuildClipPolygon(Vector2 currentPosition, Vector2 previousPosition)
    {
        // If we want to turn this back into a non-convex shape (to retain a tool's special shape, e.g.), this can probably be done by subtracting both polygons (at current and previous pos) from convex hull. This should likely result in a middle shape that we can then somehow ad bacck into the two polys. Pretty fiddly.

        clipPolygon.Clear();

        List<Vector2> points = new List<Vector2>();

        for (int i = 0; i < toolShape.Length; i++)
        {
            points.Add((previousPosition + toolShape[i]));
            points.Add((currentPosition + toolShape[i]));
        }
        // If necessary, remove duplicate points before running convex hull algorithm.
        UnityEngine.Profiling.Profiler.BeginSample("Convex Hull");
        clipPolygon = ConvexHull(points);
        UnityEngine.Profiling.Profiler.EndSample();

    }

    void UpdatePosition()
    {
        previousPosition = transform.position;
    }

    Polygon ConvexHull(List<Vector2> points)
    {
        // For future reference, this is a Jarvis convex hull algorithm.
        // TODO: Handle colinear points. This may not be a problem but it's a good idea to cover.

        int numberOfPoints = points.Count;
        int numberofPointsSquared = numberOfPoints * numberOfPoints;

        if (numberOfPoints < 3)
        {
            Debug.LogError("Point list must contain at least 3 points to be converted to convex hull.", this);
        }

        Polygon hull = new Polygon();

        // Find leftmost point
        int indexOfLeftmostPoint = 0;
        for (int i = 0; i < numberOfPoints; i++)
        {
            if (points[i].x < points[indexOfLeftmostPoint].x)
            {
                indexOfLeftmostPoint = i;
            }
        }

        // Set leftmost point to be index 0.
        points.Swap(0, indexOfLeftmostPoint);
        indexOfLeftmostPoint = 0;

        //Add leftmost point as we are certain that this will be a point on the hull.
        int confirmedCandidate = indexOfLeftmostPoint;
        int currentCandidate;
        int numLoops = 0;
        int loopDuration = numberOfPoints;
        Vector2 newestPoint = points[confirmedCandidate];
        do
        {
            numLoops++;
            // Convert to Vector2i for clipperlib usage.
            hull.Add(newestPoint.ToVector2i());
            currentCandidate = (confirmedCandidate + 1) % loopDuration; // Because the highest index in a list of length n is n-1, (x+1)%n will return 0 as next points if current point is n-1. Otherwise returns x+1.

            // Loop over all points and measure the angle between newestAcceptedPoint-i and newestAcceptedPoint-currentSampledPoint. A negative angle means that i is more counterclockwise in relation to newestAcceptedPoint than nextSamplePoint is. In this case we set nextSamplePoint to be i and contine measuring from there.
            for (int i = 0; i < loopDuration; i++)
            {
                if (Vector2.SignedAngle(points[currentCandidate] - newestPoint, points[i] - newestPoint) < 0)
                {
                    currentCandidate = i;
                }
            }
            // Now we are certain that currentSampledPoint is as far counterclockwise as we can get, so we set newestAcceptedPoint to currentSampledPoint.
            confirmedCandidate = currentCandidate;
            newestPoint = points[confirmedCandidate];

            if (confirmedCandidate != indexOfLeftmostPoint)
            {
                // Move the confirmed vector to the end of the list
                points.Swap(confirmedCandidate, loopDuration - 1);
                // Make sure we only visit points that aren't already on the hull.
                loopDuration--;
            }


            // Debugging stuff starts here.
            //            string s = @"Hull point count: {0}
            //Newest accepted: {1}
            //Current sample: {2}
            //Leftmost: {3}
            //Points:
            //{4}
            //Current/Prev pos: {5}, {6}";
            //            string lo = "";
            //            for (int i = 0; i < points.Count; i++)
            //            {
            //                lo += points[i].ToString("F5");
            //                lo += "\n";
            //            }
            //            Debug.Log("Looped for " + numLoops);
            //            Debug.Log(string.Format(s, hull.Count, confirmedCandidate, currentCandidate, indexOfLeftmostPoint, lo, transform.position.ToString("F5"), previousPosition.ToString("F5")));

            // A Jarvis algorithm should only take O(nh) to complete, in worst case that means O(n^2) if every point is a valid hull point. For now, let's assume that it will always finish in O(n^2) time.
            if (numLoops > numberofPointsSquared)
            {
                string msg = "Building convex hull was not completed in {0} time. Check if loop is completing correctly.";
                Debug.LogError(string.Format(msg, numberofPointsSquared));
                break;
            }
            // The absolute maximum number of points that the hull can contain is 2n. It will usually be less, but if we reach this amount, something is wrong.
            if (hull.Count >= toolShape.Length * 2)
            {
                string msg = "Hull now contains the maximum of points: {0}. Check if loop is completing correctly.";
                Debug.LogError(string.Format(msg, numberOfPoints * 2));
                break;
            }
        } while (confirmedCandidate != indexOfLeftmostPoint); // Run loop until we arrive back at leftmost point.
        return hull;
        
    }
    #endregion

    #region Callbacks
    private void Awake()
    {
        if (controller == null) controller = GameManager.Instance.carvingController;
        if (ps == null) ps = GetComponentInChildren<ParticleSystem>();
        ps.Stop();
        poly = GetComponent<PolygonCollider2D>();
        SetToolShape(poly.points);
        UpdatePosition();
        BuildBounds();
        BuildClipPolygon(transform.position, previousPosition);
        StartCoroutine(nameof(StopContactingWhenNotMoving), .5f);
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.hasChanged)
        {
            hasMoved = true;
            transform.hasChanged = false;
        }
        else
        {
            hasMoved = false;
        }

        if (hasMoved)
        {
            BuildClipPolygon(transform.position, previousPosition);
            BuildBounds(transform.position, previousPosition);

            UnityEngine.Profiling.Profiler.BeginSample("CutterWithinBounds");
            if (controller.CutterWithinBounds(this))
            {
                if (!contacting)
                {
                    BeginContact();
                }
            }
            else
            {
                if (contacting)
                {
                    EndContact();
                }
            }
            UnityEngine.Profiling.Profiler.EndSample();
            //lr.positionCount = clipPolygon.Count + 1;
            //for (int i = 0; i < lr.positionCount; i++)
            //{
            //    lr.SetPosition(i, clipPolygon[i % clipPolygon.Count].ToVector3());
            //}
            UpdatePosition();
        }


        if (contacting)
        {
            controller.Clip(this);
        }

        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        for (int i = 0; i < clipPolygon.Count; i++)
        {
            Gizmos.DrawLine(clipPolygon[i].ToVector3(), clipPolygon[(i + 1) % clipPolygon.Count].ToVector3());
        }
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(bounds.center, bounds.size);
    }
    #endregion
}
