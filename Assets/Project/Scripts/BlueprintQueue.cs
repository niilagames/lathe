﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BlueprintQueue", menuName = "Blueprints/BlueprintQueue", order = 3)]
public class BlueprintQueue : ScriptableObject
{
    #region Declarations
    public BlueprintDatabase blueprintDatabase; // Potentially keep reference to this one somewhere else.
    public List<int> queue;
    public Blueprint CurrentBlueprint
    {
        get
        {
            if (IsEmpty()) return null;
            return blueprintDatabase.GetBlueprint(queue[0]);
        }
    }
    #endregion

    #region Methods
    public void SaveQueue()
    {

    }

    public void LoadQueue(List<int> savedQueue) // This will likely have to happen in saveloadmanager
    {
        queue = savedQueue;
    }

    public bool ContainsBlueprint(Blueprint blueprint)
    {
        return queue.Contains(blueprintDatabase.GetID(blueprint));
    }

    public bool ContainsID(int id)
    {
        return queue.Contains(id);
    }

    public bool IsEmpty()
    {
        return queue.Count == 0;
    }

    public void RemoveOldest()
    {
        if (IsEmpty())
        {
            Debug.LogError("Blueprint queue is empty.", this);
            return;
        }
        queue.RemoveAt(0);
    }
    #endregion


}
