﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager
{
    public LatheState CurrentState { get; private set; }

    public void Initialize(LatheState startingState)
    {
        CurrentState = startingState;
        LatheEvents.OnStateEnter?.Invoke(CurrentState);
        startingState.Enter();
        LatheEvents.OnStatePostEnter?.Invoke(CurrentState);
        startingState.PostEnter();
    }

    public void ChangeState(LatheState newState)
    {
        LatheEvents.OnStateExit?.Invoke(CurrentState);
        CurrentState.Exit();

        CurrentState = newState;
        LatheEvents.OnStateEnter?.Invoke(CurrentState);
        newState.Enter();
        LatheEvents.OnStatePostEnter?.Invoke(CurrentState);
    }
}
