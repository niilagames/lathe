﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class GameManager : MonoBehaviour
{
    #region Singleton
    private static GameManager instance;
    public static GameManager Instance { get { return instance; } }
    #endregion

    #region Declarations
    public Camera gameCamera;
    public StateManager stateManager;
    public LathePreGameState pregameState;
    public LathePlayState playState;
    public LatheEvaluatingState evaluatingState;
    public LatheEndingState endingState;
    public CarvingController carvingController;
    public BlueprintQueue blueprintQueue;
    public ScoreThresholdSettings scoreThresholdSettings;
    [BoxGroup("Gameplay objects")]
    public GameObject blueprintDisplayObject;
    [BoxGroup("Gameplay objects")]
    public GameObject evaluationObject;

    #endregion

    #region Methods
    void Initialize()
    {
        stateManager = new StateManager();
        pregameState = new LathePreGameState(carvingController, stateManager);
        playState = new LathePlayState(carvingController, stateManager);
        evaluatingState = new LatheEvaluatingState(carvingController, stateManager);
        endingState = new LatheEndingState(carvingController, stateManager);
        stateManager.Initialize(pregameState);
    }

    public void EnterNextState()
    {
        stateManager.CurrentState.EnterNextState();
    }

    void PrintStateEnter(LatheState state)
    {
        //Debug.Log("enter: " + state.GetType());
    }

    void PrintStatePostEnter(LatheState state)
    {
        //Debug.Log("postenter: " + state.GetType());
    }

    void PrintStateExit(LatheState state)
    {
        //Debug.Log("exit: " + state.GetType());
    }
    #endregion

    #region Callbacks
    private void Start()
    {
        #region Singleton Initialization
        if (instance != null && instance != this)
        {
            Destroy(this);
            Debug.LogError("Multiple instances of " + GetType().Name + " was found. Please make sure you only create one instance! The newest instance was destroyed.");
        }
        else
        {
            instance = this;
        }
        #endregion
        Initialize(); // At some point this will be an event callback instead.
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        LatheEvents.OnStateEnter += PrintStateEnter;
        LatheEvents.OnStatePostEnter += PrintStatePostEnter;
        LatheEvents.OnStateExit += PrintStateExit;
    }

    private void OnDisable()
    {
        LatheEvents.OnStateEnter -= PrintStateEnter;
        LatheEvents.OnStatePostEnter -= PrintStatePostEnter;
        LatheEvents.OnStateExit -= PrintStateExit;
        instance = null;
    }
    #endregion
}
