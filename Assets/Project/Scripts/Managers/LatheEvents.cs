﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LatheEvents
{

    public delegate void ShapeEvent();
    public static ShapeEvent OnShapeChanged;

    public delegate void ThresholdEvent();
    public static ThresholdEvent ThresholdSettingsChanged;

    public delegate void LatheStateEvent(LatheState state);
    public static LatheStateEvent OnStateEnter;
    public static LatheStateEvent OnStatePostEnter;
    public static LatheStateEvent OnStateExit;

}
