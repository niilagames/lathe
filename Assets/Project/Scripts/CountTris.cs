﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static BMesh;
using UnityEngine.UI;

public class CountTris : MonoBehaviour
{
    #region Declarations
    public CarvingSubjectMesh generator;
    public CarvingController controller;
    public BMesh mesh;
    string display = "{0} verts, {1} tris";
    Text infoText;
    #endregion

    #region Methods
    
    #endregion

    #region Callbacks
    void Start()
    {
        mesh = generator.mesh;
        infoText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        mesh = generator.mesh;

        int faceTris = controller.circleSegments * 2;
        int cylinderTris = (mesh.faces.Count - faceTris) * 2;
        int numOftris = faceTris + cylinderTris;

        infoText.text = string.Format(display, mesh.vertices.Count, numOftris);
    }
    #endregion
}
