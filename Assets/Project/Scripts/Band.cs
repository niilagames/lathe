﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Band
{
    public float InnerLimit { get; set; }
    public float OuterLimit { get; set; }
    public float OuterValue { get; set; }
    public float InnerValue { get; set; }

    public Band(float innerDistance, float outerDistance, float innerValue, float outerValue)
    {
        InnerLimit = innerDistance;
        OuterLimit = outerDistance;
        InnerValue = innerValue;
        OuterValue = outerValue;
    }

    public float MaxValue()
    {
        return InnerValue;
    }

    public float MinValue()
    {
        return OuterValue;
    }

    public float ClosestValue(float distance)
    {
        float t = Mathf.InverseLerp(OuterLimit, InnerLimit, distance);
        if (t >= 0.5f)
        {
            return InnerValue;
        }
        else return OuterValue;
    }

    public float LerpedValue(float distance)
    {
        float t = Mathf.InverseLerp(OuterLimit, InnerLimit, distance);
        float val = Mathf.Lerp(OuterValue, InnerValue, t);
        //Debug.Log(OuterLimit +  ", " + InnerLimit + ", " + distance + ", " + t + ", " + val);
        return val;
    }

    public bool Contains(float distance)
    {
        return (InnerLimit <= distance && distance <= OuterLimit);
    }
}
