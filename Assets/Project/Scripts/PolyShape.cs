﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PolygonCollider2D)), ExecuteAlways]
public class PolyShape : MonoBehaviour
{
    #region Declarations
    public Vector2[] points;
    public Color color = Color.white;
    public float vertexRadius = 0.05f;
    public PolygonCollider2D poly;
    public Vector2[] polypoints;
    #endregion

    #region Methods
    void CalculatePoints()
    {
        polypoints = poly.points;

        points = ScaledPoints(TranslatePoints(polypoints));
    }

    public Vector2[] ScaledPoints(Vector2[] points)
    {
        Vector2[] scaledPoints = new Vector2[points.Length];

        for (int i = 0; i < scaledPoints.Length; i++)
        {
            scaledPoints[i] = new Vector3(transform.localScale.x * points[i].x, transform.localScale.y * points[i].y, 0);
        }

        return scaledPoints;
    }

    public Vector2[] TranslatePoints(Vector2[] points)
    {
        Vector2[] translatedPoints = new Vector2[points.Length]; 

        for (int i = 0; i < translatedPoints.Length; i++)
        {
            translatedPoints[i] = (Vector2)transform.position + points[i];
        }

        return translatedPoints;
    }

    #endregion

    #region Callbacks
    private void Update()
    {
        if (polypoints != poly.points)
        {
            CalculatePoints();
        }
    }

    void Start()
    {  
        CalculatePoints();
    }

    private void OnValidate()
    {
        CalculatePoints();
    }

    // Update is called once per frame
    private void OnDrawGizmos()
    {
        Gizmos.color = color;
        if (points.Length > 0)
        {
            for (int i = 0; i < points.Length; i++)
            {
                Gizmos.DrawSphere(points[i], vertexRadius);
                Gizmos.DrawLine(points[i], points[(i + 1) % points.Length]);
            }
        }
    }
    #endregion
}
