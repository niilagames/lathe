﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

[CreateAssetMenu(fileName = "BlueprintDatabase", menuName = "Blueprints/BlueprintDatabase", order = 3)]
public class BlueprintDatabase : SerializedScriptableObject
{
    // Convert to SerializedScriptableObject when imported into Robbi.
    // For now, this database assumes that nothing will be added into the database at runtime.

    #region Declarations
    [SerializeField, DictionaryDrawerSettings(KeyLabel = "ID", ValueLabel = "Blueprint")]
    public Dictionary<int, Blueprint> _database;
    #endregion

    #region Methods
    public Blueprint GetBlueprint(int id)
    {
        if (_database.ContainsKey(id))
        {
            return _database[id];
        }
        else
        {
            Debug.LogError("Blueprint with ID " + id + " does not exist in database.", this);
            return null;
        }
    }

    public int GetID(Blueprint shape)
    {
        if (_database.ContainsValue(shape))
        {
            int myKey = _database.FirstOrDefault(x => x.Value == shape).Key;
            return myKey;
        }
        else
        {
            Debug.LogError("Blueprint does not exist in database.", this);
            return -1;
        }
    }
    #endregion

    #region Callbacks
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion
}
