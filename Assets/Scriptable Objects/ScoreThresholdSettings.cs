﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

[CreateAssetMenu(fileName = "ScoreThresholdSettings", menuName = "ScoreThresholdSettings")]
public class ScoreThresholdSettings : ScriptableObject
{
    #region Declarations
    [Tooltip("The minimum score required to count as a match.")]
    public float acceptedScore;
    [Tooltip("Distance limits for value bands. Value is interpreted as the value to return inside of the band with this limit as outer limit.")] // This doesn't quite make sense.
    public ScoreThreshold[] limits;
    Band[] bands;
    #endregion

    #region Classes, Structs, Enums


    #endregion

    #region Methods
    public float GetValue(float distance)
    {
        // This could potentially just use CurrentBand.
        float minDist = 0;
        float maxDist = 0;
        for (int i = 0; i < limits.Length; i++)
        {
            maxDist += limits[i].distance;
            if (distance >= minDist && distance <= maxDist)
            {
                return limits[i].value;
            }
            minDist = maxDist;
        }
        return limits[limits.Length - 1].value; // If we've reached this point, no band is further out than the current accuracy distance. Return furthest band's value.
    }

    /// <summary>
    /// Returns which band a point lies within if it lies a certain distance from the blueprint.
    /// </summary>
    /// <param name="distance"></param>
    /// <returns></returns>
    public Band CurrentBand(float distance)
    {
        for (int i = 0; i < bands.Length; i++)
        {
            Band band = bands[i];
            if (band.Contains(distance))
            {
                return band;
            }
        }
        return null;
    }

    /// <summary>
    /// Builds an array of bands containing a lower+upper distance limit and their associated values. Assumes that the input values are the "max" value of their associated distance limit (i.e. that this is the value you're getting if you're inside of this band).
    /// </summary>
    public void BuildBands()
    {
        // Very inefficient - is doing something in three loops that should probably be done in one, but I can't wrap my head around it right now for some reason. With so few limits it should be fine right now. Rework if needed.

        ScoreThreshold[] mapIncludingExtremes = new ScoreThreshold[limits.Length + 2];
        float distanceCumulative = 0;
        mapIncludingExtremes[0] = new ScoreThreshold(0, limits[0].value);
        mapIncludingExtremes[mapIncludingExtremes.Length - 1] = new ScoreThreshold(float.MaxValue, 0);
        bands = new Band[mapIncludingExtremes.Length - 1];
        for (int i = 0; i < limits.Length; i++)
        {
            distanceCumulative += limits[i].distance;
            mapIncludingExtremes[i + 1] = new ScoreThreshold(distanceCumulative, limits[i].value);
        }
        for (int i = 0; i < mapIncludingExtremes.Length - 1; i++)
        {
            ScoreThreshold limit = mapIncludingExtremes[i];
            ScoreThreshold nextLimit = mapIncludingExtremes[i + 1];
            limit.value = nextLimit.value;
        }
        for (int i = 0; i < mapIncludingExtremes.Length - 1; i++)
        {
            ScoreThreshold limit = mapIncludingExtremes[i];
            ScoreThreshold nextLimit = mapIncludingExtremes[i + 1];
            bands[i] = new Band(limit.distance, nextLimit.distance, limit.value, nextLimit.value);
        }

    }
    #endregion

    #region Callbacks
    private void OnValidate()
    {
        LatheEvents.ThresholdSettingsChanged?.Invoke();
    }
    #endregion
}
