﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Blueprints/Blueprint")]
public class Blueprint : ScriptableObject
{
    #region Declarations

    public Vector2 offset;
    public float scale;

    public Vector2[] points;
    public Vector2[] TransformedPoints
    {
        get
        {
            Vector2[] transformedPoints = new Vector2[points.Length];
            for (int i = 0; i < transformedPoints.Length; i++)
            {
                transformedPoints[i] = points[i] * scale + offset;
            }

            return transformedPoints;
        }
    }

    private BMesh bmesh;

    #endregion

    #region Methods
    public BMesh MeshFromShape(Blueprint shape)
    {
        BMesh _mesh = new BMesh();
        var _points = TransformedPoints;
        BMesh.Vertex[] edge1 = new BMesh.Vertex[_points.Length];
        BMesh.Vertex[] edge2 = new BMesh.Vertex[_points.Length];
        for (int i = 0; i < _points.Length; i++)
        {
            Vector3 p = _points[i];
            p.y = Mathf.Abs(p.y); // Ensure that edge 1 gets the positive y value.
            edge1[i] = _mesh.AddVertex(p.x, p.y, p.z);
            edge2[i] = _mesh.AddVertex(p.x, -p.y, p.z);
        }
        _mesh.AddFaceStrip(edge1, edge2);
        return _mesh;
    }
    #endregion
}
